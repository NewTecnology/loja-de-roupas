﻿using Loja_de_Roupas.DB.Diferenciais.IbmVoice;
using Loja_de_Roupas.Telas.Cliente;
using Loja_de_Roupas.Telas.Estoque;
using Loja_de_Roupas.Telas.Financeiro;
using Loja_de_Roupas.Telas.Fornecedor;
using Loja_de_Roupas.Telas.Funcionário;
using Loja_de_Roupas.Telas.Pedido;
using Loja_de_Roupas.Telas.Produto;
using Loja_de_Roupas.Telas.RH;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
            VerificarPermissoes();
            toolStripStatusLabel1.Text = LogandoUsuario.UsuarioLogado.Usuario;
            toolStripStatusLabel4.Text = System.DateTime.Now.ToShortTimeString();
        }
        void VerificarPermissoes()
        {
            if (LogandoUsuario.UsuarioLogado.Admin == false)
            {
                if (LogandoUsuario.UsuarioLogado.RH == false)
                {
                    folhaDePagamentoToolStripMenuItem.Enabled = false;
                    cadastrarToolStripMenuItem.Enabled = false;
                    consultarToolStripMenuItem.Enabled = false;
                }

                if (LogandoUsuario.UsuarioLogado.Vendas == false)
                {

                    clienteToolStripMenuItem.Enabled = false;
                    pedidoToolStripMenuItem1.Enabled = false;

                }

                if (LogandoUsuario.UsuarioLogado.Logistica == false)
                {
                    consultarToolStripMenuItem8.Enabled = false;
                }

                if (LogandoUsuario.UsuarioLogado.Compras == false)
                {
                    fornecedorToolStripMenuItem.Enabled = false;
                    produtoToolStripMenuItem.Enabled = false;
                    pedidoToolStripMenuItem.Enabled = false;



                }

                if (LogandoUsuario.UsuarioLogado.Financeiro == false)
                {
                    fluxoDeCaixaToolStripMenuItem.Enabled = false;
                    gastosToolStripMenuItem.Enabled = false;

                }

            }


        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarFuncionario funcionario = new CadastrarFuncionario();
            funcionario.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CadastrarFolhaDePagamento tela = new CadastrarFolhaDePagamento();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ConsultarFolhaDePagamento tela = new ConsultarFolhaDePagamento();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Ponto tela = new Ponto();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            CadastrarFornecedor tela = new CadastrarFornecedor();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CadastrarProduto tela = new CadastrarProduto();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ConsultarProduto tela = new ConsultarProduto();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Telas.Pedido_de_Compra.CadastrarPedido tela = new Telas.Pedido_de_Compra.CadastrarPedido();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Telas.Pedido_de_Compra.ConsultarPedido tela = new Telas.Pedido_de_Compra.ConsultarPedido();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Telas.Pedido_de_Compra.CadastrarProduto tela = new Telas.Pedido_de_Compra.CadastrarProduto();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Telas.Vendas.ConsultarProduto tela = new Telas.Vendas.ConsultarProduto();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            CadastrarCliente tela = new CadastrarCliente();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            this.Hide();
        }

        private void cadastrarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            CadastrarPedido tela = new CadastrarPedido();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            ConsultarPedido tela = new ConsultarPedido();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Estoque tela = new Estoque();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            ConsultarFluxoDeCaixa tela = new ConsultarFluxoDeCaixa();
            tela.Show();
            this.Hide();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Deseja realmente sair?", "Quatro Estações",
                             MessageBoxButtons.YesNo,
                             MessageBoxIcon.Question);

            if (r == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel4.Text = System.DateTime.Now.ToShortTimeString();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void produtoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pedidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void fluxoDeCaixaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            ConsultarPonto tela = new ConsultarPonto();
            tela.Show();
            this.Hide();
        }

        private void contextMenuStrip5_Opening(object sender, CancelEventArgs e)
        {

        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarGastosToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void consultarGastosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void cadastrarToolStripMenuItem6_Click_1(object sender, EventArgs e)
        {
            CadastrarGastos tela = new CadastrarGastos();
            tela.Show();
            this.Hide();
        }

        private void consultarToolStripMenuItem5_Click_1(object sender, EventArgs e)
        {
            ConsultarGastos tela = new ConsultarGastos();
            tela.Show();
            this.Hide();
        }
        IbmVoiceApi ibmApi = new IbmVoiceApi();
        private void pictureBox7_Click(object sender, EventArgs e)
        {
            ibmApi.IniciarOuvir();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            try
            {
                lblProcess.Visible = true;
                string texto = ibmApi.PararOuvir();
                ibmApi.Verificar(texto);
                this.Hide();
                lblProcess.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Quatro estações");
                lblProcess.Visible = false;
            }
        }
    }
}
