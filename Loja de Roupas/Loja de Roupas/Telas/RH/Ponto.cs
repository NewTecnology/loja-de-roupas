﻿using Loja_de_Roupas.DB.RH.Ponto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Financeiro
{
    public partial class Ponto : Form
    {
        public Ponto()
        {
            InitializeComponent();
            lblDiaAtual.Text = DateTime.Now.ToShortDateString();
            label4.Text = LogandoUsuario.UsuarioLogado.NomeFuncionario;
            label5.Text = Convert.ToString(LogandoUsuario.UsuarioLogado.Id);
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        PontoDTO dto = new PontoDTO();

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            if (lblDiaAtual.Text == Convert.ToString(dto.Data) && label4.Text == LogandoUsuario.UsuarioLogado.NomeFuncionario)
            {
                MessageBox.Show("Esse funcionário já cadastrou o ponto");
            }
            else
            {
                int horas = dateTimePicker4.Value.Hour - dateTimePicker1.Value.Hour;
                int totalhoras = horas;
                TimeSpan horasalmoco = (dateTimePicker3.Value - dateTimePicker2.Value);
                int totalhorasalmoco = Convert.ToInt32(horasalmoco.TotalHours);
                PontoDTO ponto = new PontoDTO();
                ponto.Data = Convert.ToDateTime(lblDiaAtual.Text);
                ponto.Entrada = dateTimePicker1.Value;
                ponto.IdaAlmoco = dateTimePicker2.Value;
                ponto.VoltaAlmoco = dateTimePicker3.Value;
                ponto.Saida = dateTimePicker4.Value;
                ponto.HorasTrabalhadasDia = totalhoras - totalhorasalmoco;
                ponto.IdFuncionario = Convert.ToInt32(label5.Text);

                PontoBusiness business = new PontoBusiness();
                business.Salvar(ponto);

                EnviarMensagem("Ponto cadastrado com sucesso.");

            }

            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();

        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void dateTimePicker1_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
    
}
