﻿using Loja_de_Roupas.DB.Funcionário;
using Loja_de_Roupas.DB.RH.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Financeiro
{
    public partial class CadastrarFolhaDePagamento : Form
    {
        public CadastrarFolhaDePagamento()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            FuncionarioDTO dto = new FuncionarioDTO();
            string nome = dto.NomeFuncionario;


            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Consultar(nome);

            
            comboBox1.ValueMember = nameof(FuncionarioDTO.Id);
            comboBox1.DisplayMember = nameof(FuncionarioDTO.NomeFuncionario);
            comboBox1.DataSource = lista;

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label46_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FuncionarioDTO func = comboBox1.SelectedItem as FuncionarioDTO;

            label6.Text = Convert.ToString(func.Id);
            label30.Text = Convert.ToString(func.ValeTransporte);
            label20.Text = Convert.ToString(func.Salario);
            label32.Text = Convert.ToString(func.ValeAlimentacao);
            label34.Text = Convert.ToString(func.ValeRefeicao);
            label36.Text = Convert.ToString(func.Convenio);
            label27.Text = Convert.ToString(func.Salario);

            label45.Text = Convert.ToString("0,00");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Horas Trabalhadas

            decimal SalarioNominal = Convert.ToDecimal(label27.Text);

            int horastrabalhadasdia = 9 * 4;
            int horastrabalhadasultimodia = horastrabalhadasdia + 8;
            int HorasTrabalhadasMes = horastrabalhadasultimodia * 5;


            decimal ValorHoraTrabalhada = SalarioNominal / HorasTrabalhadasMes;
            ValorHoraTrabalhada = Math.Round(ValorHoraTrabalhada, 2);

            //Atrasos 
            decimal HorasAtraso = ValorHoraTrabalhada * numericUpDown5.Value;

            //Faltas

            decimal faltas = SalarioNominal / 30;
            decimal valorfaltas = faltas * numericUpDown4.Value;

            //Valor Hora Extra Dia
            decimal PorcentagemHoraExtra = (ValorHoraTrabalhada * 50) / 100;
            decimal ValorHoraExtra = ValorHoraTrabalhada + PorcentagemHoraExtra;
            decimal HorasExtras = ValorHoraExtra * numericUpDown1.Value;

            if (HorasExtras >= 1)
            {
                decimal horasextrasdia = HorasExtras;

            }
            else
            {
                label29.Text = "0,00";
            }


            //Horas extras no feriado

            decimal PorcentagemHoraExtraferiado = (ValorHoraTrabalhada * 100) / 100;
            decimal ValorHoraExtraferiado = ValorHoraTrabalhada + PorcentagemHoraExtraferiado;
            decimal HorasExtrasFeriado = ValorHoraExtraferiado * numericUpDown2.Value;


            if (HorasExtrasFeriado >= 1)
            {
                decimal horasextrasdomingo = HorasExtrasFeriado;

            }
            else
            {
                label29.Text = "0,00";
            }



            decimal resultado = HorasExtras + HorasExtrasFeriado;
            resultado = Math.Round(resultado, 2);
            label29.Text = Convert.ToString(resultado);


            //Salario Base do INSS
            decimal SalarioBruto = SalarioNominal + HorasExtras - HorasAtraso - valorfaltas;
            decimal SalarioBaseINSS = SalarioBruto;

            if (SalarioBaseINSS <= 1659.38m)
            {
                decimal salario = (SalarioBaseINSS * 8) / 100;
                salario = Math.Round(salario, 2);
                label40.Text = Convert.ToString(salario);
            }
            else if (SalarioBaseINSS >= 1659.39m && SalarioBaseINSS <= 2765.66m)
            {
                decimal salario = (SalarioBaseINSS * 9) / 100;
                salario = Math.Round(salario, 2);
                label40.Text = Convert.ToString(salario);

            }
            else if (SalarioBaseINSS >= 2765.67m)
            {
                decimal salario = (SalarioBaseINSS * 11) / 100;
                salario = Math.Round(salario, 2);
                label40.Text = Convert.ToString(salario);
            }

            //Imposto de Renda 
            decimal BaseCalculoIR = SalarioBaseINSS - Convert.ToDecimal(label40.Text);

            if (BaseCalculoIR <= 1903.98m)
            {
                label42.Text = "0,00";

            }
            else if (BaseCalculoIR >= 1903.99m && BaseCalculoIR <= 2826.65m)
            {
                decimal imposto = (BaseCalculoIR * 7.5m) / 100;
                decimal reduzir = 142.80m;
                reduzir = Math.Round(reduzir, 2);
                label42.Text = Convert.ToString(reduzir);

            }
            else if (BaseCalculoIR >= 2826.66m && BaseCalculoIR <= 3751.05m)
            {
                decimal imposto = (BaseCalculoIR * 15) / 100;
                decimal reduzir = 354.80m;
                reduzir = Math.Round(reduzir, 2);
                label42.Text = Convert.ToString(reduzir);

            }
            else if (BaseCalculoIR >= 3751.06m && BaseCalculoIR <= 4664.68m)
            {
                decimal imposto = (BaseCalculoIR * 22.5m) / 100;
                decimal reduzir = 636.13m;
                reduzir = Math.Round(reduzir, 2);
                label42.Text = Convert.ToString(reduzir);
            }
            else if (BaseCalculoIR >= 4664.88m)
            {
                decimal imposto = (BaseCalculoIR * 27.5m) / 100;
                decimal reduzir = 869.36m;
                reduzir = Math.Round(reduzir, 2);
                label42.Text = Convert.ToString(reduzir);
            }

            //FGTS
            decimal fgts = (SalarioBaseINSS * 8) / 100;
            fgts = Math.Round(fgts, 2);
            label38.Text = Convert.ToString(fgts);


            //Salario Liquido
            decimal salarioliquido = SalarioNominal + Convert.ToDecimal(label29.Text) - HorasAtraso - valorfaltas - Convert.ToDecimal(label40.Text)
                                                    - Convert.ToDecimal(label42.Text) - Convert.ToDecimal(label30.Text) - Convert.ToDecimal(label38.Text);
            salarioliquido = Math.Round(salarioliquido, 2);
            label23.Text = Convert.ToString(salarioliquido);
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            FolhadePagamentoDTO dto = new FolhadePagamentoDTO();
            dto.IdFuncionario = Convert.ToInt32(label6.Text);
            dto.Salario = Convert.ToDecimal(label27.Text);
            dto.HorasExtras = Convert.ToDecimal(label29.Text);
            dto.ValeTransporte = Convert.ToDecimal(label30.Text);
            dto.ValeAlimentacao = Convert.ToDecimal(label32.Text);
            dto.ValeRefeicao = Convert.ToDecimal(label34.Text);
            dto.Convenio = Convert.ToDecimal(label36.Text);
            dto.FGTS = Convert.ToDecimal(label38.Text);
            dto.INSS = Convert.ToDecimal(label40.Text);
            dto.IR = Convert.ToDecimal(label42.Text);
            dto.SalarioFamilia = Convert.ToDecimal(label45.Text);
            dto.SalarioBruto = Convert.ToDecimal(label20.Text);
            dto.DataPagamento = dateTimePicker1.Value;
            dto.SalarioLiquido = Convert.ToDecimal(label23.Text);

            FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
            business.Salvar(dto);

            EnviarMensagem("Folha cadastrada com sucesso.");

            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void groupBox1_Enter_1(object sender, EventArgs e)
        {

        }

        private void CadastrarFolhaDePagamento_Load(object sender, EventArgs e)
        {

        }
    }
}
