﻿using Loja_de_Roupas.DB.RH.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Financeiro
{
    public partial class ConsultarFolhaDePagamento : Form
    {
        public ConsultarFolhaDePagamento()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        public void CarregarGrid()
        {
            try
            {
                FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                List<FolhadePagamentoView> dto = business.Consultar(textBox1.Text.Trim());

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = dto;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar a folha: " + ex.Message, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                FolhadePagamentoView folha = dgvProdutos.CurrentRow.DataBoundItem as FolhadePagamentoView;

                DialogResult r = MessageBox.Show("Deseja excluir a folha?", "Quatro Estações",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                    business.Remover(folha.IdFolha);
                    CarregarGrid();

                }

            }
        }
    }
}
