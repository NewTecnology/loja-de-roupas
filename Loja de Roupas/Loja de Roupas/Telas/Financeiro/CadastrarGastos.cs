﻿using Loja_de_Roupas.DB.Financeiro.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Financeiro
{
    public partial class CadastrarGastos : Form
    {
        public CadastrarGastos()
        {
            InitializeComponent();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            try
            {
                GastosDTO dto = new GastosDTO();
                dto.Nome = textBox1.Text.Trim();
                dto.Data = dateTimePicker1.Value;
                dto.Valor = numericUpDown1.Value;
                dto.Tipo = comboBox1.Text;

                GastosBusiness business = new GastosBusiness();
                business.Salvar(dto);

                MessageBox.Show("Gasto cadastrado com sucesso", "Quatro Estações");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Quatro estações",
                 MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
               

            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();

        }

        private void label8_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
