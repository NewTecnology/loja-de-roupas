﻿using Loja_de_Roupas.DB.Financeiro;
using Loja_de_Roupas.DB.Financeiro.Fluxo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Financeiro
{
    public partial class ConsultarFluxoDeCaixa : Form
    {
        public ConsultarFluxoDeCaixa()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
            FluxoBusiness business = new FluxoBusiness();
            List<FluxoDTO> lista = business.Consultar(dateTimePicker1.Value, dateTimePicker2.Value);

            dgvFluxo.AutoGenerateColumns = false;
            dgvFluxo.DataSource = lista;

        }
        private void ConsultarFluxoDeCaixa_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CarregarGrid();

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
}
