﻿using Loja_de_Roupas.DB.Logística.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Estoque
{
    public partial class Estoque : Form
    {
        public Estoque()
        {
            InitializeComponent();
        }
        void CarregarGrid()
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<EstoqueConsultarView> lista = business.Consultar(textBox3.Text.Trim());

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
}
