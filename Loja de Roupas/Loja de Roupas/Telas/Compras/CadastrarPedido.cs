﻿using Loja_de_Roupas.DB.Compras.Fornecedor;
using Loja_de_Roupas.DB.Compras.Pedido;
using Loja_de_Roupas.DB.Compras.Produto;
using Loja_de_Roupas.DB.Logística.Estoque;
using Loja_de_Roupas.Telas.Compras;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Pedido_de_Compra
{
    public partial class CadastrarPedido : Form
    {
        BindingList<ProdutoConsultarView> produtosCarrinho = new BindingList<ProdutoConsultarView>();
        public CadastrarPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(FornecedorDTO.Id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome);
            comboBox1.DataSource = lista;
        }

        void ConfigurarGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = produtosCarrinho;
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
          
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoConsultarView dto = cboProduto.SelectedItem as ProdutoConsultarView;

            int qtd = Convert.ToInt32(numericUpDown1.Value);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);

            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto2 = comboBox1.SelectedItem as FornecedorDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.IdFornecedor = dto2.Id;
                dto.Data = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                int idPedido = business.Salvar(dto, produtosCarrinho.ToList());
                
                MessageBox.Show("Pedido Salvo com sucesso", "Quatro estações", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao cadastrar o pedido: " + ex.Message, "Quatro estações",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Error);

            }

            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void CadastrarPedido_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
          
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO dto = comboBox1.SelectedItem as FornecedorDTO;
            string nome = dto.Nome;

            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoConsultarView> lista = business.ConsultarPorFornecedor(nome);

            cboProduto.ValueMember = nameof(ProdutoConsultarView.Id);
            cboProduto.DisplayMember = nameof(ProdutoConsultarView.Nome);
            cboProduto.DataSource = lista;

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}
