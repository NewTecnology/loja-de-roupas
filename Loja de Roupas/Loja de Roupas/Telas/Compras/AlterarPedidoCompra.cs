﻿using Loja_de_Roupas.DB.Compras.Fornecedor;
using Loja_de_Roupas.DB.Compras.Pedido;
using Loja_de_Roupas.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Compras
{
    public partial class AlterarPedidoCompra : Form
    {
        BindingList<ProdutoConsultarView> produtosCarrinho = new BindingList<ProdutoConsultarView>();

        public AlterarPedidoCompra()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            FornecedorDTO dto = new FornecedorDTO();

            comboBox1.ValueMember = nameof(FornecedorDTO.Id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome);
            comboBox1.DataSource = lista;

            ProdutoBusiness business2 = new ProdutoBusiness();
            List<ProdutoConsultarView> lista2 = business2.Listar();

            cboProduto.ValueMember = nameof(ProdutoConsultarView.Id);
            cboProduto.DisplayMember = nameof(ProdutoConsultarView.Nome);
            cboProduto.DataSource = lista2;

        }

        PedidoCompraConsultarView pedido;
        public void LoadScreen(PedidoCompraConsultarView dto)
        {
            this.pedido = dto;
            label5.Text = Convert.ToString(dto.Id);
            numericUpDown1.Value = dto.Quantidade;
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = pedido;
            

        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoConsultarView dto = cboProduto.SelectedItem as ProdutoConsultarView;

            int qtd = Convert.ToInt32(numericUpDown1.Value);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto2 = comboBox1.SelectedItem as FornecedorDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.IdFornecedor = dto2.Id;
                dto.Data = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                business.Alterar(dto);

                MessageBox.Show("Pedido alterado com sucesso", "Quatro Estações", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao alterar o pedido: " + ex.Message, "Quatro Estações",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Error);

            }
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
}
