﻿using Loja_de_Roupas.DB.Compras.Fornecedor;
using Loja_de_Roupas.DB.Compras.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Compras
{
    public partial class AlterarProdutoCompra : Form
    {
        public AlterarProdutoCompra()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            FornecedorDTO dto = new FornecedorDTO();

            comboBox1.ValueMember = nameof(FornecedorDTO.Id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome);
            comboBox1.DataSource = lista;

           

        }
        

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        ProdutoConsultarView produto;
 
        public void LoadScreen(ProdutoConsultarView dto)
        {
            this.produto = dto;
            label7.Text = Convert.ToString(dto.Id);
            txtProduto.Text = dto.Nome;
            numericUpDown1.Value = dto.Preco;
            numericUpDown2.Value = dto.PrecoVenda;
            textBox2.Text = dto.Marca;

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
           try
           {
              FornecedorDTO fornecedor = comboBox1.SelectedItem as FornecedorDTO;
              ProdutoDTO produto2 = new ProdutoDTO();

                produto2.Id = Convert.ToInt32(label7.Text);
                produto2.Nome = txtProduto.Text.Trim();
                produto2.Preco = numericUpDown1.Value;
                produto2.PrecoVenda = numericUpDown2.Value;
                produto2.Marca = textBox2.Text.Trim();
                produto2.IdFornecedor = fornecedor.Id;

                ProdutoBusiness business = new ProdutoBusiness();
                business.Alterar(produto2);

                EnviarMensagem("Produto alterado com sucesso.");

                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();

            }
           catch (ArgumentException ex)
           {
                MessageBox.Show(ex.Message, "Quatro estações",
                  MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO func = comboBox1.SelectedItem as FornecedorDTO;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
