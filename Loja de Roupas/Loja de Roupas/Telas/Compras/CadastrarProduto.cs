﻿using Loja_de_Roupas.DB.Compras.Fornecedor;
using Loja_de_Roupas.DB.Compras.Produto;
using Loja_de_Roupas.DB.Logística.Estoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Produto
{
    public partial class CadastrarProduto : Form
    {
        BindingList<EstoqueConsultarView> estoque = new BindingList<EstoqueConsultarView>();
        public CadastrarProduto()
        {
            InitializeComponent();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            FornecedorDTO dto = new FornecedorDTO();

            comboBox1.ValueMember = nameof(FornecedorDTO.Id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome);
            comboBox1.DataSource = lista;


        }


        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
             try
             {
                FornecedorDTO fornecedor = comboBox1.SelectedItem as FornecedorDTO;

                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = txtProduto.Text.Trim();
                dto.Preco = numericUpDown1.Value;
                dto.PrecoVenda = numericUpDown2.Value;
                dto.Marca = textBox2.Text.Trim();
                dto.IdFornecedor = fornecedor.Id;

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto,estoque.ToList());

                MessageBox.Show("Produto cadastrado com sucesso", "Quatro estações");

                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();

            }
             catch (ArgumentException ex)
             {
                MessageBox.Show(ex.Message, "Quatro estações",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);

             }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO func = comboBox1.SelectedItem as FornecedorDTO;

        }
    }
    
}
