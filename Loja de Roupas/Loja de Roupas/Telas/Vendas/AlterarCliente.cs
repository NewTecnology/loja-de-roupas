﻿using Loja_de_Roupas.DB.CorreioResponde;
using Loja_de_Roupas.DB.Vendas.Cliente;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Vendas
{
    public partial class AlterarCliente : Form
    {
        public AlterarCliente()
        {
            InitializeComponent();
        }
        ClienteDTO cliente;
        public void LoadScreen(ClienteDTO dto)
        {
            this.cliente = dto;
            label2.Text = Convert.ToString(dto.Id);
            radioButton1.Checked = dto.PessoaFisica;
            radioButton2.Checked = dto.PessoaJuridica;
            textBox3.Text = dto.Nome;
            maskedTextBox1.Text = dto.RG;
            maskedTextBox2.Text = dto.CPF;
            maskedTextBox5.Text = dto.CNPJ;
            maskedTextBox3.Text = dto.Telefone;
            maskedTextBox4.Text = dto.Celular;
            textBox1.Text = dto.Email;
            txtRua.Text = dto.Rua;
            txtCEP.Text = dto.CEP;
            txtBairro.Text = dto.Bairro;
            txtCidade.Text = dto.Cidade;
            txtEstado.Text = dto.Estado;
            textBox8.Text = dto.Descricao;

        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cadascliente = new ClienteDTO();

                if (maskedTextBox1.MaskCompleted)
                {
                    cadascliente.RG = maskedTextBox1.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("RG é obrigatório");
                }

                if (maskedTextBox2.Enabled == true)
                {
                    if (maskedTextBox2.MaskCompleted)
                    {
                        cadascliente.CPF = maskedTextBox2.Text.Trim();
                        cadascliente.CNPJ = maskedTextBox5.Text;

                    }

                    else
                    {
                        throw new ArgumentException("CPF é obrigatório");
                    }

                }

                if (maskedTextBox5.Enabled == true)
                {
                    if (maskedTextBox5.MaskCompleted)
                    {
                        cadascliente.CNPJ = maskedTextBox5.Text.Trim();
                        cadascliente.CPF = maskedTextBox2.Text;

                    }

                    else
                    {
                        throw new ArgumentException("CNPJ é obrigatório");
                    }

                }

                cadascliente.Id = Convert.ToInt32(label2.Text);
                cadascliente.Nome = textBox3.Text.Trim();
                cadascliente.DataCadastro = DateTime.Now;
                cadascliente.Telefone = maskedTextBox3.Text.Trim();
                cadascliente.Celular = maskedTextBox4.Text.Trim();
                cadascliente.Email = textBox1.Text.Trim();
                cadascliente.Rua = txtRua.Text.Trim();
                cadascliente.CEP = txtCEP.Text.Trim();
                cadascliente.Bairro = txtBairro.Text.Trim();
                cadascliente.Cidade = txtCidade.Text.Trim();
                cadascliente.Estado = txtEstado.Text.Trim();
                cadascliente.Descricao = textBox8.Text.Trim();
                cadascliente.PessoaFisica = radioButton1.Checked;
                cadascliente.PessoaJuridica = radioButton2.Checked;

                ClienteBusiness business = new ClienteBusiness();
                business.Alterar(cadascliente);

                EnviarMensagem("Cliente cadastrado com sucesso.");

                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Quatro estações",
                  MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBox2.Enabled = true;
            maskedTextBox5.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            maskedTextBox2.Enabled = false;
            maskedTextBox5.Enabled = true;
        }

        private void txtCEP_Validated(object sender, EventArgs e)
        {
            // Lê e formata o CEP do textbox
            string cep = txtCEP.Text.Trim().Replace("-", "");

            // Chama função BuscarAPICorreio
            CorreioResponde correio = BuscarAPICorreio(cep);

            // Altera os valores dos textbox com a resposta do correio
            txtRua.Text = correio.Logradouro + " - " + correio.Complemento;
            txtBairro.Text = correio.bairro;
            txtCidade.Text = correio.localidade;
            txtEstado.Text = correio.uf;

        }

        private CorreioResponde BuscarAPICorreio(string cep)
        {
            // Cria objeto responsável por conversar com uma API
            WebClient rest = new WebClient();
            rest.Encoding = Encoding.UTF8;

            // Chama API do correio, concatenando o cep
            string resposta = rest.DownloadString("https://viacep.com.br/ws/" + cep + "/json");

            // Transforma a resposta do correio em DTO
            CorreioResponde correio = JsonConvert.DeserializeObject<CorreioResponde>(resposta);
            return correio;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtCidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
