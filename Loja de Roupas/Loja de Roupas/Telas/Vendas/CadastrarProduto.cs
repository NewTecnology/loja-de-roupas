﻿using Loja_de_Roupas.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Pedido_de_Compra
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            ProdutoVendaDTO dto = new ProdutoVendaDTO();
            dto.Nome = txtProduto.Text;
            dto.Preco = Convert.ToDecimal(textBox1.Text);
            dto.Marca = textBox2.Text;

            ProdutoVendaBusiness businees = new ProdutoVendaBusiness();
            businees.Salvar(dto);

            EnviarMensagem("Produto cadastrado com sucesso.");

            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }


       

    }
}
