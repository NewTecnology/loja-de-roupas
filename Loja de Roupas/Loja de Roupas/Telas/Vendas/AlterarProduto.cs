﻿using Loja_de_Roupas.DB.Compras.Produto;
using Loja_de_Roupas.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Vendas
{
    public partial class AlterarProduto : Form
    {
        public AlterarProduto()
        {
            InitializeComponent();
        }

        private void AlterarProduto_Load(object sender, EventArgs e)
        {

        }

        ProdutoVendaDTO produto;

        public void LoadScreen(ProdutoVendaDTO dto1)
        {
            this.produto = dto1;
            label13.Text = Convert.ToString(dto1.Id);
            textBox4.Text = dto1.Nome;
            textBox5.Text = dto1.Preco.ToString();
            textBox3.Text = dto1.Marca;

        }



        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoVendaDTO dto = new ProdutoVendaDTO();
                dto.Id = Convert.ToInt32(label13.Text);
                dto.Nome = textBox4.Text;
                dto.Marca = textBox3.Text;
                dto.Preco = Convert.ToDecimal(textBox5.Text);

                ProdutoVendaBusiness business = new ProdutoVendaBusiness();
                business.Alterar(dto);

                EnviarMensagem("Produto alterado com sucesso.");

                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
            catch (ArgumentException ex)
            {
                EnviarMensagemErro(ex.Message);

            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Ocorreu um erro ao alterar o produto: " + ex.Message);
            }



        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }
    }
}
