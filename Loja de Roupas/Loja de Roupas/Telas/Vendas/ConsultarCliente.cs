﻿using Loja_de_Roupas.DB.Vendas.Cliente;
using Loja_de_Roupas.Telas.Vendas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Cliente
{
    public partial class ConsultarCliente : Form
    {
        public ConsultarCliente()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        public void CarregarGrid()
        {
            try
            {
                ClienteBusiness business = new ClienteBusiness();
                List<ClienteDTO> dto = business.Consultar(textBox1.Text.Trim());

                dgvClientes.AutoGenerateColumns = false;
                dgvClientes.DataSource = dto;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar o cliente: " + ex.Message, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

            }

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 9)
            {
                ClienteDTO cliente = dgvClientes.CurrentRow.DataBoundItem as ClienteDTO;

                this.Hide();
                AlterarCliente tela = new AlterarCliente();
                tela.LoadScreen(cliente);

                tela.Show();

            }

            if (e.ColumnIndex == 10)
            {
                ClienteDTO cliente = dgvClientes.CurrentRow.DataBoundItem as ClienteDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cliente?", "Quatro Estações",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(cliente.Id);
                    CarregarGrid();

                }

            }
        }
    }
}
