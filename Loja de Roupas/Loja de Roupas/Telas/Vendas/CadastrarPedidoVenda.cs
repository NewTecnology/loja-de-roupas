﻿using Loja_de_Roupas.DB.Compras.Pedido;
using Loja_de_Roupas.DB.Compras.Produto;
using Loja_de_Roupas.DB.Logística.Estoque;
using Loja_de_Roupas.DB.Plugin;
using Loja_de_Roupas.DB.Vendas.Cliente;
using Loja_de_Roupas.DB.Vendas.Pedido;
using Loja_de_Roupas.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Pedido
{
    public partial class CadastrarPedido : Form
    {
        BindingList<EstoqueConsultarView> produtosCarrinho = new BindingList<EstoqueConsultarView>();

        public CadastrarPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

        void CarregarCombos()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(ClienteDTO.Id);
            comboBox1.DisplayMember = nameof(ClienteDTO.Nome);
            comboBox1.DataSource = lista;

            EstoqueBusiness business2 = new EstoqueBusiness();
            List<EstoqueConsultarView> lista2 = business2.Listar();

            comboBox2.ValueMember = nameof(EstoqueConsultarView.Id);
            comboBox2.DisplayMember = nameof(EstoqueConsultarView.Produto);
            comboBox2.DataSource = lista2;
        }

        void ConfigurarGrid()
        {
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = produtosCarrinho;
        }


        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            try
            {
                string mensagem = produtosCarrinho.ToString();

                string a = "";
                List<string> nomes = new List<string>();
                string b = string.Empty;
                int quantidade = 1;
                b = produtosCarrinho[0].Produto;
                string c = "";
                foreach (EstoqueConsultarView item in produtosCarrinho)
                {
                    if (b == item.Produto)
                    {
                        a = quantidade.ToString() + item.Produto;
                        quantidade = quantidade + 1;
                    }
                    if (b != item.Produto)
                    {
                        if (c == "")
                        {
                            quantidade = 1;
                        }
                        c = quantidade.ToString() + item.Produto;
                        quantidade = quantidade + 1;

                    }

                }
                a = a + ", " + c;
                Email email = new Email();

                if(txtEmail.Text != string.Empty)
                {
                    email.Enviar(txtEmail.Text, a);
                }

                ClienteDTO dto2 = comboBox1.SelectedItem as ClienteDTO;

                PedidoVendaDTO dto = new PedidoVendaDTO();
                dto.IdCliente = dto2.Id;
                dto.Data = DateTime.Now;

                PedidoVendaBusiness business = new PedidoVendaBusiness();
                int idPedido = business.Salvar(dto, produtosCarrinho.ToList());

                EstoqueBusiness business2 = new EstoqueBusiness();
                List<ConsultarPedidoVendaView> lista = business.ConsultarPorId(idPedido);
                List<EstoqueDTO> estoque = business2.Listar2();

                foreach (ConsultarPedidoVendaView item in lista)
                {
                    foreach (EstoqueDTO item2 in estoque)
                    {
                        if (item.IdProduto == item2.IdProduto)
                        {
                            item2.Quantidade = item2.Quantidade - item.Quantidade;
                        }
                    }
                }

                foreach (EstoqueDTO item in estoque)
                {
                    EstoqueDTO estoquedto = new EstoqueDTO();

                    estoquedto.Id = item.Id;
                    estoquedto.IdProduto = item.IdProduto;
                    estoquedto.Quantidade = item.Quantidade;

                    business2.Alterar2(estoquedto);
                }

                MessageBox.Show("Pedido Salvo com sucesso", "Quatro estações", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao cadastrar o pedido: " + ex.Message, "Quatro estações",
                      MessageBoxButtons.OK,
                      MessageBoxIcon.Error);

            }

            
        }
        int i = 10;

       
        private void button1_Click_1(object sender, EventArgs e)
        {
            EstoqueBusiness estoquebussiness = new EstoqueBusiness();

            List<EstoqueConsultarView> estoquedto = estoquebussiness.Listar();


            EstoqueConsultarView dto = comboBox2.SelectedItem as EstoqueConsultarView;
            int produtoid = Convert.ToInt32(comboBox2.SelectedValue);
            int qtd = Convert.ToInt32(numericUpDown1.Value);

            foreach (EstoqueConsultarView item in estoquedto)
            {
                if (dto.ProdutoId == produtoid)
                {
                    if (i == 0)
                    {
                        if (dto.Quantidade < qtd)
                        {
                            MessageBox.Show("Quantidade de " + dto.Produto + " não é suficiente,atualmente temos em estoque: " + dto.Quantidade, "Quatro estações");
                        }
                        else if(dto.Quantidade == qtd)
                        {
                            for (i = 0; i < qtd; i++)

                            {
                                produtosCarrinho.Add(dto);

                            }

                            dto.Quantidade = dto.Quantidade - qtd;

                            dataGridView1.AutoGenerateColumns = false;
                            dataGridView1.DataSource = produtosCarrinho;
                            i = 0;


                        }

                        else
                        {

                            for (i = 0; i < qtd; i++)

                            {
                                produtosCarrinho.Add(dto);

                            }

                            dto.Quantidade = dto.Quantidade - qtd;

                            dataGridView1.AutoGenerateColumns = false;
                            dataGridView1.DataSource = produtosCarrinho;
                            i = 0;
                        }
                    }
                    else
                    {
                        if (dto.Quantidade < qtd)
                        {
                            MessageBox.Show("Quantidade de " + dto.Produto + " não é suficiente,atualmente temos em estoque: " + dto.Quantidade, "Quatro estações");
                        }
                        else
                        {

                            for (i = 0; i < qtd; i++)

                            {
                                produtosCarrinho.Add(dto);

                            }

                            dto.Quantidade = dto.Quantidade - qtd;

                            dataGridView1.AutoGenerateColumns = false;
                            dataGridView1.DataSource = produtosCarrinho;
                            i = 0;
                        }
                    }
                }
            }

        }
        
            
        
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            EstoqueConsultarView dto = comboBox2.SelectedItem as EstoqueConsultarView;
            numericUpDown1.Value = dto.Quantidade;

           
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClienteDTO cliente = comboBox1.SelectedItem as ClienteDTO;
            txtEmail.Text = cliente.Email;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
