﻿using Loja_de_Roupas.DB.Vendas.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Pedido
{
    public partial class ConsultarPedido : Form
    {
        public ConsultarPedido()
        {
            InitializeComponent();
        }

        void CarregarGrid()
        {
            PedidoVendaBusiness business = new PedidoVendaBusiness();
            List<ConsultarPedidoVendaView> lista = business.Consultar(dateTimePicker1.Value, dateTimePicker2.Value);

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = lista;

        }

        private void ConsultarPedido_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click_1(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                ConsultarPedidoVendaView pedido = dgvPedidos.CurrentRow.DataBoundItem as ConsultarPedidoVendaView;

                DialogResult r = MessageBox.Show("Deseja excluir o pedido?", "Quatro Estações",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    PedidoVendaBusiness business = new PedidoVendaBusiness();
                    business.Remover(pedido.Id);
                    CarregarGrid();

                }

            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
