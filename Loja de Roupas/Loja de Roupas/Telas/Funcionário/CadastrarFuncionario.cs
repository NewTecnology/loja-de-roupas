﻿using Loja_de_Roupas.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Funcionário
{
    public partial class CadastrarFuncionario : Form
    {
        public CadastrarFuncionario()
        {
            InitializeComponent();
            checkBox1.Enabled = false;
            checkBox2.Checked = true;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void CadastrarFuncionario_Load(object sender, EventArgs e)
        {
           
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void label10_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();

                if (maskedTextBox1.MaskCompleted)
                {
                    funcionario.RG = maskedTextBox1.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("RG é obrigatório");
                }

                if (maskedTextBox2.MaskCompleted)
                {
                    funcionario.CPF = maskedTextBox2.Text.Trim();

                }
                else
                {
                    throw new ArgumentException("CPF é obrigatório");
                }

                funcionario.NomeFuncionario = textBox3.Text.Trim();
                funcionario.Endereco = textBox2.Text.Trim();
                funcionario.Salario = numericUpDown1.Value;
                funcionario.ValeTransporte = numericUpDown2.Value;
                funcionario.ValeAlimentacao = numericUpDown3.Value;
                funcionario.ValeRefeicao = numericUpDown4.Value;
                funcionario.Convenio = numericUpDown5.Value;
                funcionario.Usuario = textBox13.Text.Trim();
                funcionario.Senha = txtSenha.Text.Trim();
                funcionario.Admin = checkBox1.Checked;
                funcionario.Funcionario = checkBox2.Checked;
                funcionario.RH = checkBox7.Checked;
                funcionario.Compras = checkBox6.Checked;
                funcionario.Logistica = checkBox4.Checked;
                funcionario.Vendas = checkBox5.Checked;
                funcionario.Financeiro = checkBox3.Checked;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(funcionario);

                EnviarMensagem("Funcionario cadastrado com sucesso.");

                this.Hide();
                MenuPrincipal tela = new MenuPrincipal();
                tela.Show();

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Quatro estações",
                 MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
       


        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
