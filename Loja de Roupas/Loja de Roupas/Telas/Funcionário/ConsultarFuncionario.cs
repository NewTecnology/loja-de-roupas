﻿using Loja_de_Roupas.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.Telas.Funcionário
{
    public partial class ConsultarFuncionario : Form
    {
        public ConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void ConsultarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal tela = new MenuPrincipal();
            tela.Show();
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 5)
            {
                FuncionarioDTO funcionario = dgvProdutos.CurrentRow.DataBoundItem as FuncionarioDTO;

                this.Hide();
                AlterarFuncionario tela = new AlterarFuncionario();
                tela.LoadScreen(funcionario);

                tela.Show();

            }

            if (e.ColumnIndex == 6)
            {
                FuncionarioDTO musica = dgvProdutos.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o funcionário?", "Quatro Estações",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(musica.Id);
                    CarregarGrid();

                }

            }
        }
        public void CarregarGrid()
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> dto = business.Consultar(textBox3.Text.Trim());

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = dto;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao consultar o funcionário: " + ex.Message, "Quatro Estações",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);

            }

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}
