﻿using Loja_de_Roupas.DB.Funcionário;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
           


        }

        private void btnAdd_Click_1(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtLogin.Text, txtSenha.Text);

                if (funcionario != null)
                {
                    LogandoUsuario.UsuarioLogado = funcionario;

                    MenuPrincipal menu = new MenuPrincipal();
                    menu.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Quatro Estações", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Quatro Estações",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Por favor contate o Administrador do Sistema", "Quatro Estações",
               MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
