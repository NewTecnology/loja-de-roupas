﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Plugin
{
    class Email
    {
        public void Enviar(string para, string mensagem)
        {
            Task.Factory.StartNew(() =>
            {
                // Se for usar seu email, habilite ele em: https://myaccount.google.com/lesssecureapps?pli=1
                string remetente = "lojaderoupas12@gmail.com";
                string senha = "loja12345";


                string assunto = "Compra Realizada com sucesso | Loja de Roupas Quatro Estações";
                mensagem = CriarMensagemComHtml(mensagem);

                // Configura a mensagem
                MailMessage email = new MailMessage();

                // Configura Remetente, Destinatário
                email.From = new MailAddress(remetente);
                email.To.Add(para);

                // Configura Assunto, Corpo e se o Corpo está em Html
                email.Subject = assunto;
                email.Body = mensagem;
                email.IsBodyHtml = true;

                // Adiciona Imagem no Corpo do Email
                AdicionarImagem(email);

                // Configura os parâmetros do objeto SMTP
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;


                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(remetente, senha);

                // Envia a mensagem
                smtp.Send(email);

            });
        }

        public string CriarMensagemComHtml(string mensagem)
        {
            // Lê o html do arquivo email.html
            string html = System.IO.File.ReadAllText("DB/Diferenciais/Plugin/email.html");

            // Substitui as quebras de linhas pela tag <br>
            mensagem = mensagem.Replace("\n", "<br>");

            // Coloca a mensagem no template em html
            mensagem = html.Replace("{MENSAGEM}", mensagem);

            return mensagem;
        }

        public void AdicionarImagem(MailMessage mail)
        {
            string attachmentPath = "DB/Diferenciais/Plugin/4Estações.png";
            Attachment inline = new Attachment(attachmentPath);
            inline.ContentDisposition.Inline = true;
            inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
            inline.ContentId = "icon";
            inline.ContentType.MediaType = "image/png";

            mail.Attachments.Add(inline);


        }
    }
}
