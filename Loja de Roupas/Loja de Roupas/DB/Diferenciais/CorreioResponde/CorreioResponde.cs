﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.CorreioResponde
{
    class CorreioResponde
    {
        public string cep { get; set; }
        public string Logradouro { get; set; }
        public string uf { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string Complemento { get; set; }
    }
}
