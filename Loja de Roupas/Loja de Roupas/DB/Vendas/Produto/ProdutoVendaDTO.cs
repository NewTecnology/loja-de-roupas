﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Vendas.Produto
{
    public class ProdutoVendaDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Marca { get; set;}
        public decimal Preco { get; set;}

    }
}
