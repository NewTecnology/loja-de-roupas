﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Vendas.Pedido
{
    public class ConsultarPedidoVendaView
    {
        public int Id { get; set; }
        public string Cliente{ get; set; }
        public int Quantidade { get; set; }
        public DateTime Data { get; set; }
        public decimal ValorTotal { get; set; }
        public int IdProduto { get; set; }
    }      
}
