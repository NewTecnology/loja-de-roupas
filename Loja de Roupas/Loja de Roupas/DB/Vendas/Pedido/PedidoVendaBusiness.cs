﻿using Loja_de_Roupas.DB.Compras.Produto;
using Loja_de_Roupas.DB.Logística.Estoque;
using Loja_de_Roupas.DB.Vendas.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Vendas.Pedido
{
    class PedidoVendaBusiness
    {
        public int Salvar(PedidoVendaDTO pedido, List<EstoqueConsultarView> produtos)
        {
            PedidoVendaDatabase pedidoDatabase = new PedidoVendaDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoVendaItemBusiness itemBusiness = new PedidoVendaItemBusiness();
            foreach (EstoqueConsultarView item in produtos)
            {
                PedidoItemVendaDTO itemDto = new PedidoItemVendaDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.ProdutoId;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public List<ConsultarPedidoVendaView> Consultar(DateTime inicio, DateTime fim)
        {
            PedidoVendaDatabase db = new PedidoVendaDatabase();
            return db.Consultar(inicio, fim);

        }

        public void Remover(int pedidoId)
        {
            PedidoVendaItemBusiness itemBusiness = new PedidoVendaItemBusiness();
            List<PedidoItemVendaDTO> itens = itemBusiness.ConsultarPorPedido(pedidoId);

            foreach (PedidoItemVendaDTO item in itens)
            {
                itemBusiness.Remover(item.Id);
            }

            PedidoVendaDatabase pedidoDatabase = new PedidoVendaDatabase();
            pedidoDatabase.Remover(pedidoId);

        }
        public List<ConsultarPedidoVendaView> ConsultarPorId(int idCompra)
        {
            PedidoVendaDatabase db = new PedidoVendaDatabase();
            return db.ConsultarPorId(idCompra);

        }

        public void Alterar(PedidoVendaDTO dto)
        {
            PedidoVendaDatabase db = new PedidoVendaDatabase();
            db.Alterar(dto);

        }

    }
}
