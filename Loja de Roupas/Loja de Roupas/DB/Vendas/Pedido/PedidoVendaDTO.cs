﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Vendas.Pedido
{
    class PedidoVendaDTO
    {
        public int Id { get; set; }
        public string Quantidade { get; set; }
        public DateTime Data { get; set; }
        public int IdCliente { get; set; }
        

    }
}
