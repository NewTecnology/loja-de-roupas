﻿create database LojaQuatroEstacoesDB;
USE LojaQuatroEstacoesDB;


CREATE TABLE IF NOT EXISTS tb_funcionario (
  id_funcionario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nm_funcionario VARCHAR(150) NOT NULL,
  ds_endereco VARCHAR(150) NOT NULL,
  ds_RG VARCHAR(20) NOT NULL,
  ds_CPF VARCHAR(20) NOT NULL,
  ds_usuario VARCHAR(100) NOT NULL,
  ds_senha VARCHAR(100) NOT NULL,
  vl_salario DECIMAL(16,2) NOT NULL,
  vl_vt DECIMAL(15,2),
  vl_va DECIMAL(15,2),
  vl_vr DECIMAL(15,2),
  vl_convenio DECIMAL(15,2),
  bt_admin BOOL,
  bt_funcionario BOOL,
  bt_permissao_RH BOOL,
  bt_permissao_vendas BOOL,
  bt_permissao_compras BOOL,
  bt_permissao_financeiro BOOL,
  bt_permissao_logistica BOOL
);

Insert into `LojaQuatroEstacoesDB`.`tb_funcionario`(`id_funcionario`,`nm_funcionario`,`ds_endereco`,`ds_RG`,`ds_CPF`,`ds_usuario`,`ds_senha`,`vl_salario`,`vl_vt`,`vl_va`,`vl_vr`,`vl_convenio`,`bt_admin`,`bt_funcionario`,`bt_permissao_RH`,`bt_permissao_vendas`,`bt_permissao_compras`,`bt_permissao_financeiro`,`bt_permissao_logistica`)
VALUES ('1','Admin','','','','Admin','admin12345','0','0','0','0','0',true,true,true,true,true,true,true);

CREATE TABLE IF NOT EXISTS tb_cliente (
  id_cliente INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nm_nome VARCHAR(100) NOT NULL,
  dt_data_cadastro DATE NOT NULL,
  ds_RG  VARCHAR(15) NOT NULL,
  ds_CPF VARCHAR(20),
  ds_CNPJ VARCHAR(35),
  ds_email VARCHAR(150),
  ds_telefone VARCHAR(20),
  ds_celular VARCHAR(20),
  ds_rua VARCHAR(150) NOT NULL,
  ds_CEP VARCHAR(9) NOT NULL,
  ds_bairro VARCHAR(100) NOT NULL,
  ds_cidade VARCHAR(100) NOT NULL,
  ds_estado VARCHAR(100) NOT NULL,
  bt_pessoafisica BOOL,
  bt_pessoajuridica BOOL,
  ds_cliente VARCHAR(250)
  );

select * from tb_cliente;

  CREATE TABLE IF NOT EXISTS tb_fornecedor (
  id_fornecedor INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nm_nome VARCHAR(100) NOT NULL,
  ds_telefone VARCHAR(20) NOT NULL,
  ds_email VARCHAR(100),
  ds_CNPJ VARCHAR(30) NOT NULL,
  ds_rua VARCHAR(150) NOT NULL,
  ds_CEP VARCHAR(12) NOT NULL,
  ds_bairro VARCHAR(100) NOT NULL,
  ds_cidade VARCHAR(100) NOT NULL,
  ds_estado VARCHAR(100) NOT NULL
);



CREATE TABLE IF NOT EXISTS tb_produto_compra (
  id_produto INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nm_nome_produto VARCHAR(100) NOT NULL,
  ds_marca VARCHAR(100) NOT NULL,
  vl_preco DECIMAL(20,2) NOT NULL,
  vl_preco_venda DECIMAL(20,2) NOT NULL,
  fk_id_fornecedor INT NOT NULL,
  foreign key(fk_id_fornecedor) references tb_fornecedor(id_fornecedor)
);


Create View vw_consultar_produto_compra as 
	 select tb_produto_compra.id_produto,
			tb_produto_compra.nm_nome_produto,
            tb_produto_compra.ds_marca,
            tb_produto_compra.vl_preco,
            tb_produto_compra.vl_preco_venda,
            tb_fornecedor.nm_nome
       from tb_produto_compra
       join tb_fornecedor
         on tb_produto_compra.fk_id_fornecedor = tb_fornecedor.id_fornecedor
GROUP 
		 BY tb_produto_compra.id_produto,
			tb_produto_compra.nm_nome_produto,
            tb_produto_compra.ds_marca,
            tb_produto_compra.vl_preco,
            tb_produto_compra.vl_preco_venda,
            tb_fornecedor.nm_nome;


CREATE TABLE IF NOT EXISTS tb_Pedido_Compra (
  idPedidoCompra INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  dt_compra date,
  fk_fornecedor INT NOT NULL,
  foreign key(fk_fornecedor) references tb_fornecedor(id_fornecedor)
);

select * from tb_Pedido_Compra;

Create table tb_compra_item (
id_compra_item INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
fk_produto_compra INT NOT NULL,
fk_pedido_compra INT NOT NULL,
foreign key (fk_produto_compra) references tb_produto_compra(id_produto),
foreign key (fk_pedido_compra) references tb_Pedido_Compra(idPedidoCompra)
);

select * from tb_compra_item;

create VIEW vw_pedido_consultar AS 
	SELECT tb_Pedido_Compra.idPedidoCompra,
		   tb_fornecedor.nm_nome,
           tb_Pedido_Compra.dt_compra,
           tb_produto_compra.id_produto,
		   count(tb_compra_item.id_compra_item) 	as qtd_itens,
           sum(tb_produto_compra.vl_preco)		 	as vl_total
      FROM tb_Pedido_Compra
      JOIN tb_compra_item
        ON tb_Pedido_Compra.idPedidoCompra = tb_compra_item.fk_pedido_compra
	  JOIN tb_produto_compra
        ON tb_compra_item.fk_produto_compra = tb_produto_compra.id_produto
	  JOIN tb_fornecedor
        ON tb_fornecedor.id_fornecedor = tb_Pedido_Compra.fk_fornecedor
        
	 GROUP 
	    BY tb_Pedido_Compra.idPedidoCompra,
		   tb_fornecedor.nm_nome,
           tb_Pedido_Compra.dt_compra,
           tb_produto_compra.id_produto;

select * from vw_pedido_consultar;


CREATE TABLE IF NOT EXISTS tb_Pedido_Venda (
  idPedidoVenda INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  dt_venda date,
  fk_cliente INT NOT NULL,
  foreign key(fk_cliente) references tb_cliente(id_cliente)
);


Create table tb_venda_item (
id_venda_item INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
fk_produto_venda INT NOT NULL,
fk_pedido_venda INT NOT NULL,
foreign key (fk_produto_venda) references tb_produto_compra(id_produto),
foreign key (fk_pedido_venda) references tb_Pedido_Venda(idPedidoVenda)
);


create VIEW vw_pedido_venda_consultar AS 
	SELECT tb_Pedido_Venda.idPedidoVenda,
		   tb_cliente.nm_nome,
           tb_Pedido_Venda.dt_venda,
           tb_produto_compra.id_produto,
		   count(tb_venda_item.id_venda_item) 	as qtd_itens,
           sum(tb_produto_compra.vl_preco_venda) as vl_total
      FROM tb_Pedido_Venda
      JOIN tb_venda_item
        ON tb_Pedido_Venda.idPedidoVenda = tb_venda_item.fk_pedido_venda
	  JOIN tb_produto_compra
        ON tb_venda_item.fk_produto_venda = tb_produto_compra.id_produto
	  JOIN tb_cliente
        ON tb_cliente.id_cliente = tb_Pedido_Venda.fk_cliente
	 GROUP 
	    BY tb_Pedido_Venda.idPedidoVenda,
		   tb_cliente.nm_nome,
           tb_Pedido_Venda.dt_venda,
           tb_produto_compra.id_produto;

select * from vw_pedido_venda_consultar;

create table tb_estoque2 (
id_estoque int NOT NULL PRIMARY KEY AUTO_INCREMENT,
fk_produto_compra INT NOT NULL,
qt_estoque INT NOT NULL,
foreign key(fk_produto_compra) references tb_produto_compra(id_produto)
);

select * from tb_estoque2;

create view vw_consultar_estoque as
 select tb_estoque2.id_estoque,
        tb_produto_compra.nm_nome_produto,
		tb_estoque2.qt_estoque,
		tb_fornecedor.nm_nome,
        tb_produto_compra.ds_marca,
        tb_produto_compra.vl_preco_venda,
        tb_produto_compra.id_produto
   from tb_estoque2
   join tb_produto_compra 
     on tb_estoque2.fk_produto_compra = tb_produto_compra.id_produto
   join tb_fornecedor
	 on tb_produto_compra.fk_id_fornecedor = tb_fornecedor.id_fornecedor
GROUP 
   BY   tb_estoque2.id_estoque,
        tb_produto_compra.nm_nome_produto,
		tb_estoque2.qt_estoque,
		tb_fornecedor.nm_nome,
        tb_produto_compra.ds_marca,
        tb_produto_compra.vl_preco_venda,
        tb_produto_compra.id_produto;

        
select * from vw_consultar_estoque;


CREATE TABLE IF NOT EXISTS tb_ponto (
  id_ponto INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  dt_ponto DATE NOT NULL,
  hr_entrada TIME NOT NULL,
  hr_almoco_ida TIME NOT NULL,
  hr_almoco_volta TIME NOT NULL,
  hr_saida TIME NOT NULL,
  hrs_trabalhadas_dia INT,
  fk_ponto_funcionario INT NOT NULL,
  FOREIGN KEY (fk_ponto_funcionario) references tb_funcionario(id_funcionario)
);



create VIEW vw_consultar_ponto as 
select tb_ponto.id_ponto,
	   tb_funcionario.nm_funcionario,
       tb_ponto.dt_ponto,
       tb_ponto.hr_entrada,
       tb_ponto.hr_almoco_ida,
       tb_ponto.hr_almoco_volta,
       tb_ponto.hr_saida,
       tb_ponto.hrs_trabalhadas_dia
  from tb_ponto
  join tb_funcionario
	on tb_ponto.fk_ponto_funcionario = tb_funcionario.id_funcionario
GROUP
   by  tb_ponto.id_ponto,
	   tb_funcionario.nm_funcionario,
       tb_ponto.dt_ponto,
       tb_ponto.hr_entrada,
       tb_ponto.hr_almoco_ida,
       tb_ponto.hr_almoco_volta,
       tb_ponto.hr_saida,
	   tb_ponto.hrs_trabalhadas_dia;

select * from vw_consultar_ponto;



CREATE TABLE IF NOT EXISTS tb_folha_de_pagamento (
  id_folha_de_pagamento INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  vl_salario DECIMAL(16,2) NOT NULL,
  vl_vt DECIMAL(16,2),
  vl_vr DECIMAL(16,2),
  vl_va DECIMAL(16,2),
  vl_convenio DECIMAL(16,2),
  vl_fgts DECIMAL(16,2) NOT NULL,
  vl_inss DECIMAL(16,2),
  vl_ir DECIMAL(16,2),
  vl_horasextras DECIMAL(16,2),
  vl_salariofamilia DECIMAL(10,2),
  vl_bruto DECIMAL(16,2) NOT NULL,
  vl_liquido DECIMAL(16,2) NOT NULL,
  dt_pagamento DATE,
  fk_id_funcionario INT NOT NULL,
  foreign key(fk_id_funcionario) references tb_funcionario(id_funcionario)
 );



create view vw_consultar_folha as 
select tb_folha_de_pagamento.id_folha_de_pagamento,
	   tb_funcionario.nm_funcionario,
       tb_folha_de_pagamento.vl_bruto,
       tb_folha_de_pagamento.vl_horasextras,
       tb_folha_de_pagamento.dt_pagamento,
       tb_folha_de_pagamento.vl_liquido
  from tb_folha_de_pagamento
  join tb_funcionario
	on tb_folha_de_pagamento.fk_id_funcionario = tb_funcionario.id_funcionario
GROUP 
   by  tb_folha_de_pagamento.id_folha_de_pagamento,
	   tb_funcionario.nm_funcionario,
       tb_folha_de_pagamento.vl_bruto,
       tb_folha_de_pagamento.vl_horasextras,
       tb_folha_de_pagamento.dt_pagamento,
       tb_folha_de_pagamento.vl_liquido;
       
 create table tb_gastos(
 id_gasto INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nm_gasto VARCHAR(100),
 dt_gasto DATE,     
 ds_tipo  VARCHAR(150),
 vl_gasto DECIMAL(15,2)
 );
 

 
 /*VIEW DE GANHOS */


create view vw_ganhos as
select date(dt_venda)		dt_referencia,
			   sum(vl_preco_venda) 	vl_total
		  from tb_Pedido_Venda		pe
		  join tb_venda_item	pi
			on pe.idPedidoVenda 	= pi.fk_pedido_venda
		  join tb_produto_compra		p
			on pi.fk_produto_venda		= p.id_produto
		 group
			by date(dt_venda);

select * from vw_ganhos;


/*VIEW DE DESPESAS COM FOLHA DE PAGAMENTO */


create view vw_despesas as
            select sum(vl_total) vl_total, dt_referencia
            from
            (
select date(dt_compra)			dt_referencia,
			   sum(vl_preco) 	vl_total
		  from tb_Pedido_Compra		c
		  join tb_compra_item	ci
			on c.idPedidoCompra 		= ci.fk_pedido_compra
		  join tb_produto_compra		p
			on p.id_produto 		= ci.fk_produto_compra
		 group
			by date(dt_compra)
         UNION
         select date(dt_gasto)	dt_referencia,
			    sum(vl_gasto)		vl_total
		   from tb_gastos
		  group
		  	 by date(dt_gasto)
             UNION
         select date(dt_pagamento)	dt_referencia,
			    sum(vl_bruto)		vl_total
		   from tb_folha_de_pagamento
		  group
		  	 by dt_pagamento)despesa
             GROUP BY dt_referencia;

select * from vw_despesas;
		  
        

select * from vw_despesas;

/*VIEW CORDENADORA QUE PUXA AS DUAS VIEW E FAZ A CONTA DO SALDO*/


create view vw_consultar_fluxodecaixa as
   SELECT dt_referencia,
		   ifnull(vl_total_ganhos, 0)		vl_total_ganhos,
           ifnull(vl_total_despesas, 0)		vl_total_despesas,
		   ifnull(vl_total_ganhos - vl_total_despesas,0) vl_saldo
      FROM 
        (
			SELECT g.dt_referencia,
				   g.vl_total		vl_total_ganhos,
                   d.vl_total		vl_total_despesas
			  FROM vw_ganhos		g		
		 LEFT JOIN vw_despesas		d
				ON g.dt_referencia  = d.dt_referencia
			 UNION
			SELECT d.dt_referencia,
				   g.vl_total		vl_total_ganhos,
                   d.vl_total		vl_total_despesas
			  FROM vw_ganhos		g		
		 RIGHT JOIN vw_despesas		d
				ON g.dt_referencia  = d.dt_referencia
        ) tb_Fluxo;
        
        select * from vw_consultar_fluxodecaixa;