﻿using Loja_de_Roupas.DB.Compras.Produto;
using Loja_de_Roupas.DB.Logística.Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loja_de_Roupas.DB.Compras.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto, List<EstoqueConsultarView> estoque)
        {
            if (dto.Nome == string.Empty)
            {
               throw new ArgumentException("Nome do produto é obrigatório");
            }

            if (dto.Preco == 0)
            {
               throw new ArgumentException ("Preço de compra deve ser maior que 0");
            }

            if (dto.PrecoVenda == 0)
            {
                throw new ArgumentException("Preço de venda deve ser maior que 0");
            }

            if (dto.Marca == string.Empty)
            {
                throw new ArgumentException("Marca do produto é obrigatório");
            }


            ProdutoDatabase produtoDatabase = new ProdutoDatabase();
            int idProduto = produtoDatabase.Salvar(dto);

            EstoqueBusiness itemBusiness = new EstoqueBusiness();

            EstoqueDTO itemDto = new EstoqueDTO();
            itemDto.IdProduto = idProduto;
            itemDto.Quantidade = 0;

            itemBusiness.Salvar(itemDto);


            return idProduto;


        }
        public List<ProdutoConsultarView> ConsultarPorId(int idProduto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.ConsultarPorId(idProduto);

        }
        public List<ProdutoConsultarView> Consultar(string produto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar(produto);
        }

        public List<ProdutoConsultarView> ConsultarPorFornecedor(string fornecedor)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.ConsultarPorFornecedor(fornecedor);
        }



        public List<ProdutoConsultarView> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Listar();
        }

        public void Remover(int id)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            db.Remover(id);
        }

        public void Alterar(ProdutoDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome do produto é obrigatório");
            }

            if (dto.Preco == 0)
            {
                throw new ArgumentException("Preço de compra deve ser maior que 0");
            }

            if (dto.PrecoVenda == 0)
            {
                throw new ArgumentException("Preço de venda deve ser maior que 0");
            }

            if (dto.Marca == string.Empty)
            {
                throw new ArgumentException("Marca do produto é obrigatório");
            }

            ProdutoDatabase db = new ProdutoDatabase();
            db.Alterar(dto);
        }



    }

}



