﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Logística.Estoque
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Salvar(dto);

        }

        public List<EstoqueConsultarView> Consultar(string produto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Consultar(produto);

        }

        public List<EstoqueConsultarView> Listar()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar();
        }

        public List<EstoqueDTO> Listar2()
        {
            EstoqueDatabase db = new EstoqueDatabase();
            return db.Listar2();
        }


        public void Alterar(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Alterar(dto);
        }

        public void Alterar2(EstoqueDTO dto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.Alterar2(dto);
        }

        public void AdicionarNoEstoque(int qtd, int idProduto)
        {
            EstoqueDatabase db = new EstoqueDatabase();
            db.AdicionarNoEstoque(qtd, idProduto);
        }
    }
}
