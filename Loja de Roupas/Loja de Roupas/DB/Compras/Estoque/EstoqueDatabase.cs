﻿using Loja_de_Roupas.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Logística.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script = @"INSERT into tb_estoque2 (fk_produto_compra,qt_estoque) VALUES(@fk_produto_compra,@qt_estoque)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_produto_compra", dto.IdProduto));
            parms.Add(new MySqlParameter("qt_estoque", dto.Quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<EstoqueDTO> Listar2()
        {
            string script = @"SELECT * FROM tb_estoque2";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Id = reader.GetInt32("id_estoque");
                dto.IdProduto = reader.GetInt32("fk_produto_compra");
                dto.Quantidade = reader.GetInt32("qt_estoque");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<EstoqueConsultarView> Consultar(string produto)
        {
            string script = @"SELECT * FROM vw_consultar_estoque WHERE nm_nome_produto like @nm_nome_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueConsultarView> lista = new List<EstoqueConsultarView>();
            while (reader.Read())
            {
                EstoqueConsultarView dto = new EstoqueConsultarView();
                dto.Id = reader.GetInt32("id_estoque");
                dto.Produto = reader.GetString("nm_nome_produto");
                dto.Quantidade = reader.GetInt32("qt_estoque");
                dto.Fornecedor = reader.GetString("nm_nome");
                dto.Marca = reader.GetString("ds_marca");
                dto.Valor = reader.GetDecimal("vl_preco_venda");
                dto.ProdutoId = reader.GetInt32("id_produto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<EstoqueConsultarView> Listar()
        {
            string script = @"SELECT * FROM vw_consultar_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueConsultarView> lista = new List<EstoqueConsultarView>();
            while (reader.Read())
            {
                EstoqueConsultarView dto = new EstoqueConsultarView();
                dto.Id = reader.GetInt32("id_estoque");
                dto.Produto = reader.GetString("nm_nome_produto");
                dto.Quantidade = reader.GetInt32("qt_estoque");
                dto.Fornecedor = reader.GetString("nm_nome");
                dto.Marca = reader.GetString("ds_marca");
                dto.Valor = reader.GetDecimal("vl_preco_venda");
                dto.ProdutoId = reader.GetInt32("id_produto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque2
                                 SET id_estoque = @id_estoque,
                                qt_estoque = @qt_estoque
                  WHERE fk_produto_compra  = @fk_produto_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", dto.Id));
            parms.Add(new MySqlParameter("qt_estoque", dto.Quantidade));
            parms.Add(new MySqlParameter("fk_produto_compra", dto.IdProduto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Alterar2(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque2
                                 SET id_estoque = @id_estoque,
                                     qt_estoque = @qt_estoque
                  WHERE fk_produto_compra = @fk_produto_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", dto.Id));
            parms.Add(new MySqlParameter("qt_estoque", dto.Quantidade));
            parms.Add(new MySqlParameter("fk_produto_compra", dto.IdProduto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }



        public void AdicionarNoEstoque(int qtd, int idProduto)
        {
            string script = @"UPDATE tb_estoque2
                                 SET qt_estoque = qt_estoque + @qt_estoque
                               WHERE fk_produto_compra = @fk_produto_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qt_estoque", qtd));
            parms.Add(new MySqlParameter("fk_produto_compra", idProduto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void RetiraNoEstoque(int qtd, int idProduto)
        {
            string script = @"UPDATE tb_estoque2
                                 SET qt_estoque = qt_estoque - @qt_estoque
                               WHERE fk_produto_compra = @fk_produto_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("qt_estoque", qtd));
            parms.Add(new MySqlParameter("fk_produto_compra", idProduto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


    }
}
