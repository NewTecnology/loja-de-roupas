﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Compras.Pedido
{
    public class PedidoDTO
    {
        public int Id { get; set; }
        public int IdFornecedor { get; set; }
        public DateTime Data { get; set; }
    }
}
