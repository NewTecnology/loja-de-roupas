﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loja_de_Roupas.DB.Compras.Pedido
{
   public class PedidoItemDTO
   {
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public int IdProduto { get; set; }

   }
}
